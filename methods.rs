struct Thing {
  name: String,
}

impl Thing {
  fn print(&self) {
    println!("Thing(name: {})",
        self.name);
  }
}

let x: Thing = ...;
x.print();
