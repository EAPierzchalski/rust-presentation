#[derive(Debug)]
#[repr(C)]
pub struct Thing {
    a: u8,
    b: u32,
    c: u8,
}

#[no_mangle]
pub extern "C" fn print_thing(t: &Thing) {
    println!("{:?}", t);
}
