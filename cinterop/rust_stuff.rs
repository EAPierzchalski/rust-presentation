extern "C" {
    fn defined_in_c(x: u32) -> u32;
}

#[no_mangle]
pub extern "C" fn callable_from_c(x: u32) -> u32 {
    unsafe {
        let y = defined_in_c(x);
        return y * y;
    }
}
