#!/bin/bash

mkdir out
rustc --crate-type=cdylib rust_stuff.rs --out-dir out
gcc -Lout c_stuff.c -lrust_stuff -o out/cinterop

rustc --crate-type=cdylib rust_structs.rs --out-dir out
gcc -Lout c_structs.c -lrust_structs -o out/cstructs
