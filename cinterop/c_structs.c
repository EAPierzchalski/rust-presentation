#include <stdint.h>
#include <stdio.h>

typedef struct thing_t {
    uint8_t a;
    uint32_t b;
    uint8_t c;
} thing_t;

extern void print_thing(thing_t *thing);

void main() {
    thing_t x = {
        .a = 3,
        .b = 4,
        .c = 5,
    };

    print_thing(&x);
}
