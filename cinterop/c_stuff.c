#include <stdio.h>
#include <stdint.h>

extern uint32_t defined_in_c(uint32_t x) {
    return x + 1;
}

extern uint32_t callable_from_c(uint32_t);

int main() {
    printf("%d", callable_from_c(3));
}
