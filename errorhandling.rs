// '?' is shorthand for:
// match x {
//   Ok(a) => a,
//   Err(e) => return
//       Err(From::from(e)),
// }
let x =
    u8::from_str("100")?;
let y =
    u8::from_str("300")?;
let z = x.checked_add(y)
    .ok_or("overflow!")?;
