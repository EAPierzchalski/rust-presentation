let x =
    RefCell::new(1usize);

let r1 = x.borrow();

// runtime error: mutable
// reference while there's
// another immutable one
let mut r2 =
    x.borrow_mut();
*r2 = 2;
