let mut sock =
    UnixStream::connect(
        "/neato/socket")?;
let msg: Protocol = ...;

// send over the socket using
// (say) MessagePack or JSON
serde_json::to_writer(
    &mut sock, msg)?;

// wait for a response
match serde_json::from_reader(
    &mut sock)?
{ ... }
