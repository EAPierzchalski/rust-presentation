enum FancyEnum {
  NormalVariant,
  CoolTuple(String, u32),
  NeatoStruct {
    field_1: u8,
    field_2: *const i8,
  },
}

let thing: FancyEnum = ...;
match thing {
    CoolTuple(a, b) => ...,
    _ => ...,
}
