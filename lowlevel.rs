let ptr: *mut usize =
    0xdeadbeef;
unsafe {
  *ptr = 1;
}

let mut v = vec!(1u32, 2, 3);
unsafe {
  let x =
      v.get_unchecked_mut(3);
  *x = 4;
}
