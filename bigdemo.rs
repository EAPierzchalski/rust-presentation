#[derive(Serialize,
    Deserialize)]
enum Protocol {
  GetKey {
    name: String},
  Key(Vec<u8>),
  GetValue {
    auth: Vec<u8>,
    name: String},
}

