// 'Send' but not 'Sync'
let x =
    RefCell::new("floop");

// 'Send' and 'Sync'
let y =
    Mutex::new(x.clone());

// Neither 'Send' nor
//   'Sync'
let z =
    Arc::new(x.clone());
