let x = "beep".to_string();

let f = |y: String|  {
  println!("{} {}", x, y);
}

f("boop".to_string());
f("burp".to_string());
