#!/bin/bash

rm -f main.log
latexmk -pdf \
    -pdflatex="lualatex %O --shell-escape %S" \
    -latex="lualatex %O --shell-escape %S" \
    main.tex
