fn borrows(x: &Thing);
fn mutates(x: &mut Thing);
fn takes(x: Thing);

struct HasReference<'r> {
  a_ref: &'r mut u32,
}
