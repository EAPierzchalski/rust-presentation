trait Iterator {
  type Item;

  fn next(&mut self)
      -> Option<Self::Item>;
}

impl<A> Iterator
  for MyCoolStruct<A>
{
  type Item = A;
  fn next(&mut self) -> ...
}
