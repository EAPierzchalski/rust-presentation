#[derive(Debug)]
struct Thing {
  foo: u32,
}

let t = Thing {
  foo: 1337,
};

// Thing { foo: 1337 }
println!("{:?}", t);
