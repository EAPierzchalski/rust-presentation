extern "C" {
  fn defined_in_c(x: u32) -> u32;
}

#[no_mangle]
pub extern "C"
  fn callable_from_c(x: u32)
    -> u32 { ... }

#[repr(C)]
struct SameFieldLayoutAsC {
  small: u8,
  aligned_big: u64,
  small_badly_placed: u8,
}
