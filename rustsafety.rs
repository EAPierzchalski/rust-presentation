let mut v =
    vec!(1u8, 2, 3);

for r in v.iter_mut() {
  *r = 0xf;
  // compile error: shouldn't
  // mutate v while there's a
  // reference into it
  v.push(4);
}
