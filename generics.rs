enum Option<A> {
  Some(A),
  None,
}

impl<A> Option<A> {
  fn unwrap(self) -> A {
    match self {
      Some(a) => return a,
      None => panic!("whoops"),
    }
  }
}
